let restApi = "https://nowaunoweb.azurewebsites.net/api/game";
let game = {};
let topCard = {};
let player1 = {};
let player1Cards = {};
let player2 = {};
let player2Cards = {};
let player3 = {};
let player3Cards = {};
let player4 = {};
let player4Cards = {};
let p1 = "";
let p2 = "";
let p3 = "";
let p4 = "";
let players = {};
let nextPlayer = "";


let INIT = {
  onReady: function() {
    $("#inputPlayer2").hide();
    $("#inputPlayer3").hide();
    $("#inputPlayer4").hide();
    $("#welcome").fadeIn(2500);
    INIT.setPlayers();
  },

  setPlayers: function() {
    $("#inputPlayer1 input").keyup(function(e){
      if ($(this).val() == ""){
        $(".invalid-feedback").show();
      }else {
        $(".invalid-feedback").hide();
        if (e.keyCode == 13){
          p1 = $(this).val();
          $("#inputPlayer1").hide()
          $("#inputPlayer2").show();
          $("#p1").html(p1);
        }
      }
    });

    $("#inputPlayer2 input").keyup(function(e){
      if ($(this).val() == ""){
        $(".invalid-feedback").show();
      }else {
        $(".invalid-feedback").hide();
        if (e.keyCode == 13){
          p2 = $(this).val();
          $("#inputPlayer2").hide()
          $("#inputPlayer3").show();
          $("#p2").html(p2);
        }
      }
    });

    $("#inputPlayer3 input").keyup(function(e){
      if ($(this).val() == ""){
        $(".invalid-feedback").show();
      }else {
        $(".invalid-feedback").hide();
        if (e.keyCode == 13){
          p3 = $(this).val();
          $("#inputPlayer3").hide()
          $("#inputPlayer4").show();
          $("#p3").html(p3);
        }
      }
    });

    $("#inputPlayer4 input").keyup(function(e){
      if ($(this).val() == ""){
        $(".invalid-feedback").show();
      }else {
        $(".invalid-feedback").hide();
        if (e.keyCode == 13){
          p4 = $(this).val();
          $("#p4").html(p4);
          $("#inputPlayer4").hide();
          $("#welcome").hide(1000);
          $("#startGame").show(1000);

          players = [p1, p2, p3, p4];
        }
      }
      INIT.postPlayers(players);
    });
  },

  postPlayers: function(players) {
    $.ajax({
      method: "POST",
      url: restApi + "/start",
      data: JSON.stringify(players),
      contentType: "application/json",
      success: function(data) {

        game = data;
        gameId = game.Id;
        topCard = game.TopCard;
        nextPlayer = game.NextPlayer;
        INIT.createPlayers();

        console.log("GameInfo: ", game);
        $("#startGame").hide(10000);
        $("#game").fadeIn(8000);

        INIT.showTopCard(1500);
        INIT.showPlayerCards(1500);
      },
      error: function(err) {
        console.log(err);
      }
    });
  },

  createPlayers: function(){
    player1 = $(game.Players).get(0);
    player2 = $(game.Players).get(1);
    player3 = $(game.Players).get(2);
    player4 = $(game.Players).get(3);

    player1Cards = $(player1.Cards);
    player2Cards = $(player2.Cards);
    player3Cards = $(player3.Cards);
    player4Cards = $(player4.Cards);

  },



  showPlayerCards: function(){

    $(player1Cards).each(function(){
      let color = this.Color.charAt(0);
      let value = this.Value;
      if (value == 10){
        value = "d2";
      }else if (value == 11){
        value = "s";
      }else if(value == 12){
        value = "r";
      }else if (value == 13) {
        color = "";
        value = "wd4";
      }
      let card = color+value;
      let li = $("<li class='list-inline-item'></li>");
      li.append("<img src='http://nowaunoweb.azurewebsites.net/content/cards/"+card+".png'></img>");
      $("#player1").append(li);
    });

    $(player2Cards).each(function(){
      let color = this.Color.charAt(0);
      let value = this.Value;
      if (value == 10){
        value = "d2";
      }else if (value == 11){
        value = "s";
      }else if(value == 12){
        value = "r";
      }else if (value == 13) {
        color = "";
        value = "wd4";
      }
      let card = color+value;
      let li = $("<li class='list-inline-item'></li>");
      li.append("<img src='http://nowaunoweb.azurewebsites.net/content/cards/"+card+".png'></img>");
      $("#player2").append(li);
    });

    $(player3Cards).each(function(){
      let color = this.Color.charAt(0);
      let value = this.Value;
      if (value == 10){
        value = "d2";
      }else if (value == 11){
        value = "s";
      }else if(value == 12){
        value = "r";
      }else if (value == 13) {
        color = "";
        value = "wd4";
      }
      let card = color+value;
      let li = $("<li class='list-inline-item'></li>");
      li.append("<img src='http://nowaunoweb.azurewebsites.net/content/cards/"+card+".png'></img>");
      $("#player3").append(li);
    });

    $(player4Cards).each(function(){
      let color = this.Color.charAt(0);
      let value = this.Value;
      if (value == 10){
        value = "d2";
      }else if (value == 11){
        value = "s";
      }else if(value == 12){
        value = "r";
      }else if (value == 13) {
        color = "";
        value = "wd4";
      }
      let card = color+value;
      let li = $("<li class='list-inline-item'></li>");
      li.append("<img src='http://nowaunoweb.azurewebsites.net/content/cards/"+card+".png'></img>");
      $("#player4").append(li);

      INIT.focusOnNextPlayer();
    });

  },

  focusOnNextPlayer: function(){
    if(nextPlayer == p1){
      $("#player1").on("mouseenter", "li", function(e){
        $(e.target).addClass("highlight");
      });
      $("#player1").on("mouseleave", "li", function(e) {
        $(e.target).removeClass("highlight");
      });
      $("#p1").addClass("color");
      $("#p2").removeClass("color");
      $("#p3").removeClass("color");
      $("#p4").removeClass("color");
    }else if (nextPlayer == p2) {
      $("#player2").on("mouseenter", "li", function(e){
        $(e.target).addClass("highlight");
      });
      $("#player2").on("mouseleave", "li", function(e) {
        $(e.target).removeClass("highlight");
      });
      $("#p2").addClass("color");
      $("#p3").removeClass("color");
      $("#p4").removeClass("color");
      $("#p1").removeClass("color");
    }else if (nextPlayer == p3) {
      $("#player3").on("mouseenter", "li", function(e){
        $(e.target).addClass("highlight");
      });
      $("#player3").on("mouseleave", "li", function(e) {
        $(e.target).removeClass("highlight");
      });
      $("#p3").addClass("color");
      $("#p4").removeClass("color");
      $("#p1").removeClass("color");
      $("#p2").removeClass("color");
    }else if (nextPlayer == p4) {
      $("#player4").on("mouseenter", "li", function(e){
        $(e.target).addClass("highlight");
      });
      $("#player4").on("mouseleave", "li", function(e) {
        $(e.target).removeClass("highlight");
      });
      $("#p4").addClass("color");
      $("#p1").removeClass("color");
      $("#p2").removeClass("color");
      $("#p3").removeClass("color");
    }
  },

  showTopCard: function(){
    let color = game.TopCard.Color.charAt(0);
    let value = game.TopCard.Value;
    if (value == 10){
      value = "d2";
    }else if (value == 11){
      value = "s";
    }else if(value == 12){
      value = "r";
    }else if (value == 13) {
      color = "";
      value = "wd4";
    }
    let card = color+value;
    $(".playground").append("<img src='http://nowaunoweb.azurewebsites.net/content/cards/"+card+".png'></img>");

    $(".playground img").addClass("imgdisplayed");



  },



}








$(function() {
  console.log("document ready");
  INIT.onReady();
});
